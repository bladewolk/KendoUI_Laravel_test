<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css"/>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css"/>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css"/>

    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
</head>
<body>

<div id="example">
    <div id="grid"></div>

    <script>

        $(document).ready(function () {
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/api/test",
                        type: "get"
                    },
                    create: {
                        url: "/api/test",
                        type: "post"
                    },
                    update: {
                        url: function (item) {
                            return "/api/test/" + item.id;
                        },
                        type: "patch"
                    },
                    destroy: {
                        url: function (item) {
                            return "/api/test/" + item.id
                        },
                        type: "delete"
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: {editable: false, nullable: true},
                            description: {type: "string", validation: {required: true}},
                            label_id: {type: "string", validation: {required: true}},
                            text: {type: "string", validation: {required: true}},
                            language_id: {type: "number", validation: {required: true}}
                        }
                    }
                }
            });

            $("#grid").kendoGrid({
                dataSource: dataSource,
                pageable: false,
                height: 550,
                toolbar: ["create"],
                columns: [
                    {field: "id", title: "translation_id"},
                    {field: "description"},
                    {field: "label_id"},
                    {field: "text"},
                    {
                        field: "language_id",
                        width: "180px",
                        editor: categoryDropDownEditor,
                        template: "#=language_id#"
                    },
                    {command: ["edit", "destroy"], title: "actions", width: "300px"}],
                editable: "inline"
            });

            function categoryDropDownEditor(container, options) {
                console.log(options);
                $('<input required name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: true,
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: {
                            transport: {
                                read: "/api/langs"
                            }
                        }
                    });
            }
        });
    </script>
</div>
</body>
</html>
