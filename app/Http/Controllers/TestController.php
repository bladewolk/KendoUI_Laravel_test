<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Translation;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function langs()
    {
        $langs = Language::get();

        $res = [];

        foreach ($langs as $lang) {
            $res[] = [
                'text' => $lang->id,
                'value' => $lang->id
            ];
        }

        return $res;
    }

    public function index()
    {
        $tanslations = Translation::get();

        return $tanslations->makeHidden(['created_at', 'updated_at']);
    }

    public function show($id)
    {
        dd($id);
    }

    public function store(Request $request)
    {
        $item = Translation::create($request->all());

        return $item;
    }

    public function update(Request $request, $id)
    {
        Translation::find($id)
            ->update($request->all());
    }

    public function destroy($id)
    {
        Translation::find($id)->delete();
    }
}
