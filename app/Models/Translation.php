<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'description',
        'label_id',
        'text',
        'language_id'
    ];

    protected $guarded = [
        'language_id'
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function getTranslationIdAttribute()
    {
        return $this->id;
    }
}
