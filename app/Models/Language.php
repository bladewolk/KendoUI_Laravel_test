<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = [
        'code', 'label'
    ];

    public function translations()
    {
        return $this->hasMany(Translation::class);
    }
}
